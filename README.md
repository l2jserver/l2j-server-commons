L2J Server: Commons
===

L2J Server: Commons is a library used mostly for database and networking shared code.


Links
---

- [Web Site](http://www.l2jserver.com)

- [Forums](http://www.l2jserver.com/forum/)

- [Discord](https://discord.gg/AzHh7e2Sej)

- [Trello](https://trello.com/b/qjLoH966)

- [@l2jserver](https://twitter.com/l2jserver)